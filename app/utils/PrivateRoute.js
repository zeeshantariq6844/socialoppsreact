import React from 'react';
import { Route } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const auth = localStorage.getItem('authentication');
  const authentication = JSON.parse(auth);
  if (!authentication) {
    window.location = '/login';
    return null;
  }
  if (rest.tokenResponse.error === true) {
    rest.dispatchLogout();
  }
  const dt = new Date().getTime();
  if (authentication) {
    const tokenTime = new Date(authentication.expiry);
    const now = new Date(dt);
    if (tokenTime < now) {
      rest.tokenRefresh(authentication.refresh);
    }
  }
  return (
    // Show the component only when the user is logged in
    // Otherwise, redirect the user to /signin page
    <Route {...rest} render={props => <Component {...props} />} />
  );
};

export default PrivateRoute;
