export const DATE_FORMAT = 'YYYY-MM-DD';
export const addDays = (currentDate, days) => {
  const date = new Date(currentDate);
  date.setDate(date.getDate() + days);
  return date;
}

export const formatNumber = num => {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}
