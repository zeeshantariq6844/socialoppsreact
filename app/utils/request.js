/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  return response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  if (response.status === 401) {
    const error = new Error();
    error.response = response;
    error.code = 401;
    throw error;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */

export default function request(
  url,
  method = 'GET',
  data,
  formData = false,
  headers = [],
) {
  if (!formData) {
    headers.push({
      'Content-Type': 'application/json',
    });
  }
  const headersData = headers.reduce((acc, item) => ({ ...acc, ...item }), {});
  const op = { method, headers: headersData };
  if (data !== '') {
    op.body = data;
  }
  return fetch(url, op)
    .then(checkStatus)
    .then(parseJSON);
}
