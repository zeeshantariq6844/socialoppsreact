import React from 'react';
import './footer.css';
import { Navbar, Nav, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function Footer() {
  return (
    <div className="divFooter">
      <Link to="/menu">
        <Navbar.Brand href="#home" style={{ color: '#007BFF' }}>
          &copy; Social OPPS
        </Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Form inline />
      </Navbar.Collapse>
    </div>
  );
}

export default Footer;
