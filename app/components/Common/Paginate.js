import React from 'react';
import ReactPaginate from 'react-paginate';
import { PER_PAGE_NUMBER } from '../../config';

function Paginate({ count, handlePageChange }) {
  if (count === 0) return null;

  return (
    <ReactPaginate
      previousLabel="<<"
      nextLabel=">>"
      breakLabel="............"
      breakClassName="break-me"
      pageCount={Math.ceil(count / PER_PAGE_NUMBER)}
      marginPagesDisplayed={2}
      pageRangeDisplayed={5}
      onPageChange={handlePageChange}
      containerClassName="pagination mt-5"
      subContainerClassName="pages pagination"
      activeClassName="active"
      pageClassName="page-item"
      nextClassName="page-link"
      pageLinkClassName="page-link"
      previousClassName="page-link"
    />
  );
}

export default Paginate;
