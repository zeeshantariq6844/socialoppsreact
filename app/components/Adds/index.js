import React, { useState } from 'react';
import { Card } from 'react-bootstrap';
import Toggle from '../Toggle';
import { API_URL } from '../../config';
import { formatNumber } from '../../utils/helpers';

export default function Adds({ item, invertAdStatus, deleteAd }) {
  const [show, setShow] = useState(false);
  const [invert, setInvert] = useState(false);
  const [toggle, setToggle] = useState(item.ad_status);
  if (item.description !== null) {
    item.inventory_id.special_feature = item.description;
  }
  if (item.title !== null) {
    item.inventory_id.unit_title = item.title;
  }
  if (item.price !== null) {
    item.inventory_id.price = item.price;
  }

  const onChangeToggle = () => {
    setInvert(!invert);
  };

  const onDelete = id => {
    deleteAd(id);
  };

  const onClickYes = () => {
    setToggle(!toggle);
    setInvert(!invert);
    invertAdStatus(item, !toggle);
  };

  const Attribute = ({ name, val }) => (
    <div className="clearfix">
      <div className="pull-left">{name}:</div>
      <div className="pull-right">{val}</div>
    </div>
  );

  return (
    <div className="adcard">
      <div className="clearfix">
        <div className="adstatus">
          <span className="status">Add Status</span>
          <span className="togglebutton">
            <Toggle
              status={item}
              toggle={toggle}
              setToggle={setToggle}
              invertAdStatus={invertAdStatus}
              onChangeToggle={onChangeToggle}
            />
          </span>
        </div>
        <div>
          <hr />
        </div>
        <div className="pull-right mr-3 mb-2">
          <div className={show === false && invert === false ? '' : 'd-none'}>
            <a className="text-danger" onClick={() => setShow(!show)}>
              Delete
            </a>
          </div>
        </div>
        <div className="bg-light confirm">
          <div className={invert === true ? '' : 'd-none'}>
            Are You Sure You Want to Change the AD Status?
            <div className="clearfix">
              <button
                className="pull-left default bg-danger"
                onClick={() => setInvert(!invert)}
              >
                No
              </button>
              <button className="pull-right default" onClick={onClickYes}>
                Yes
              </button>
            </div>
          </div>
        </div>

        <div className="bg-light confirm">
          <div className={show === true ? '' : 'd-none'}>
            Are You Sure You Want to Delete the AD?
            <div className="clearfix">
              <button
                className="pull-left default bg-danger"
                onClick={() => setShow(!show)}
              >
                No
              </button>
              <button
                className="pull-right default"
                onClick={() => onDelete(item.id)}
              >
                Yes
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="description">
        <Card>
          <Card.Body>
            <Card.Title>
              {item.inventory_id && item.inventory_id.unit_title}
            </Card.Title>
            <Card.Text>
              {item.inventory_id && item.inventory_id.special_feature}
            </Card.Text>
          </Card.Body>
          <div className={item.local_photo === null ? '' : 'd-none'}>
            <Card.Img src={item.photo} />
          </div>
          <div className={item.local_photo !== null ? '' : 'd-none'}>
            <Card.Img src={`${API_URL}${item.local_photo}`} />
          </div>
          <Card.Text className="mt-4">
            <Attribute
              name="Price"
              val={formatNumber(item.inventory_id.price)}
            />
            <Attribute name="Start Date" val={item.start_date} />
            <Attribute name="End Date" val={item.end_date} />
          </Card.Text>
        </Card>
      </div>
    </div>
  );
}
