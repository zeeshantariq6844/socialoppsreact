import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { Modal, Button } from 'react-bootstrap';
import Step1 from '../ADCreateSections/section1';
import Step2 from '../ADCreateSections/section2';
import Step3 from '../ADCreateSections/section3';
import Step4 from '../ADCreateSections/section4';
import Progress from '../ProgressBar/index';
import { addDays, DATE_FORMAT } from '../../utils/helpers';
import { FB_PAGE_ID } from '../../config';

export default function CreateAdd({
  show,
  item,
  audience,
  handleShow,
  submitAd,
  adPostResponse,
}) {
  const defaultState = {
    audience: ['Facebook Page Fans'],
    geolocation: 'nationwide',
    starDate: moment(currentDate).format(DATE_FORMAT),
    endDate: '',
    length: 'Ever Green AD',
    placement: ['Facebook'],
    budgetType: 'Daily',
    terms: false,
    inventory_id: item.id,
    budgetAmount: '',
    photo: item.img_src,
    description: item.special_feature,
    title: item.unit_title,
    price: item.price,
  };
  const [section, setSection] = useState(1);
  const [progress, setProgress] = useState(20);
  const [adParameters, setadParameters] = useState(defaultState);
  const currentDate = new Date();

  useEffect(() => {
    setSection(1);
    setadParameters(defaultState);
  }, []);

  const handleOnBudgetChange = ev => {
    if (ev.target.value > 1 || /^\d?\d*$/.test(ev.target.value)) {
      setadParameters({
        ...adParameters,
        [ev.target.name]: ev.target.value,
      });
    }
  };
  const onSelectFile = form => {
    setadParameters({
      ...adParameters,
      localPhoto: form,
    });
  };
  const handleOnDateRange = (ev, type) => {
    let newDatesObj = adParameters;
    if (type === 'start') {
      newDatesObj = {
        ...adParameters,
        starDate: moment(ev).format(DATE_FORMAT),
      };
      setadParameters(newDatesObj);
    }
    if (type === 'end') {
      newDatesObj = {
        ...adParameters,
        endDate: moment(ev).format(DATE_FORMAT),
      };
      setadParameters(newDatesObj);
    }
  };

  const handleAudience = (ev, checked, value) => {
    const audienceData = adParameters.audience;
    if (checked) {
      audienceData.push(value);
      setadParameters({ ...adParameters, audience: audienceData });
    } else {
      const index = audienceData.indexOf(value);
      audienceData.splice(index, 1);
      setadParameters({ ...adParameters, audience: audienceData });
    }
  };

  const handleGeoLocation = (ev, name, value) => {
    setadParameters({ ...adParameters, [name]: value });
  };
  const handleLength = (ev, value) => {
    let newLengthObj = adParameters;
    if (value === '1 Week') {
      newLengthObj = {
        ...adParameters,
        endDate: moment(addDays(currentDate, 7)).format(DATE_FORMAT),
      };
      setadParameters(newLengthObj);
    }
    if (value === '2 Weeks') {
      newLengthObj = {
        ...adParameters,
        endDate: moment(addDays(currentDate, 14)).format(DATE_FORMAT),
      };
      setadParameters(newLengthObj);
    }
    if (value === '1 Month') {
      newLengthObj = {
        ...adParameters,
        endDate: moment(addDays(currentDate, 30)).format(DATE_FORMAT),
      };
      setadParameters(newLengthObj);
    }
    setadParameters({
      ...newLengthObj,
      length: value,
    });
  };

  const handlePlacement = (ev, checked, value) => {
    const placementData = adParameters.placement;
    if (checked) {
      placementData.push(value);
      setadParameters({ ...adParameters, placement: placementData });
    } else {
      const index = placementData.indexOf(value);
      placementData.splice(index, 1);
      setadParameters({ ...adParameters, placement: placementData });
    }
  };

  const handleOnChange = ev => {
    const { name, checked, value } = ev.target;
    if (name === 'audience') {
      handleAudience(ev, checked, value);
    }
    if (name === 'geolocation') {
      handleGeoLocation(ev, name, value);
    }
    if (name === 'length') {
      handleLength(ev, value);
    }
    if (name === 'placement') {
      handlePlacement(ev, checked, value);
    }
    if (name === 'budget') {
      setadParameters({ ...adParameters, budgetType: value });
    }
    if (name === 'terms') {
      setadParameters({ ...adParameters, terms: checked });
    }
    if (name === 'title') {
      setadParameters({ ...adParameters, title: value });
    }
  };
  const nextSection = () => {
    setSection(section + 1);
    setProgress(progress + 30);
  };
  const previousSection = () => {
    setSection(section - 1);
    setProgress(progress - 30);
  };
  const handleFormChange = ev => {
    setadParameters({ ...adParameters, [ev.target.name]: ev.target.value });
  };
  const handleADSubmit = () => {
    const finalAdParameters = new FormData();
    if (adParameters.localPhoto) {
      finalAdParameters.append('local_photo', adParameters.localPhoto);
    } else {
      finalAdParameters.append('photo', adParameters.photo);
    }
    finalAdParameters.append('audience', adParameters.audience.toString());
    finalAdParameters.append('placement', adParameters.placement.toString());
    finalAdParameters.append('budget_type', adParameters.budgetType);
    finalAdParameters.append('budget', adParameters.budgetAmount);
    finalAdParameters.append('location', adParameters.geolocation);
    finalAdParameters.append('start_date', adParameters.starDate);
    finalAdParameters.append('end_date', adParameters.endDate);
    finalAdParameters.append('fb_page_id', FB_PAGE_ID);
    finalAdParameters.append('ad_status', false);
    finalAdParameters.append('inventory_id', adParameters.inventory_id);
    finalAdParameters.append('user_id', 1);
    finalAdParameters.append('description', adParameters.description);
    finalAdParameters.append('title', adParameters.title);
    finalAdParameters.append('price', adParameters.price);
    submitAd(finalAdParameters);
  };
  return (
    <Modal
      show={show}
      onHide={handleShow}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="m-2">
        {adPostResponse.error === true && (
          <div className="alert alert-danger">Ad Posting Failed</div>
        )}
        <Progress progress={progress} />
      </Modal.Header>
      <Modal.Body className="m-2">
        <div id="step1" className={section === 1 ? '' : 'd-none'}>
          <Step1
            onChange={handleOnChange}
            item={item}
            onSelectFile={onSelectFile}
            handleFormChange={handleFormChange}
            adParameters={adParameters}
          />
        </div>
        <div id="step2" className={section === 2 ? '' : 'd-none'}>
          <Step2
            audience={audience}
            handleOnChange={handleOnChange}
            adParameters={adParameters}
          />
        </div>
        <div id="step3" className={section === 3 ? '' : 'd-none'}>
          <Step3
            handleOnChange={handleOnChange}
            adParameters={adParameters}
            handleOnDateRange={handleOnDateRange}
          />
        </div>
        <div id="step4" className={section === 4 ? '' : 'd-none'}>
          <Step4
            handleOnChange={handleOnChange}
            adParameters={adParameters}
            handleOnBudgetChange={handleOnBudgetChange}
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
        {section === 1 && (
          <Button className="grey" onClick={handleShow}>
            X Close
          </Button>
        )}
        {section !== 1 && (
          <Button className="default" onClick={previousSection}>
            Back
          </Button>
        )}
        {section !== 4 && (
          <Button className="default right" onClick={nextSection}>
            Next >
          </Button>
        )}
        {section === 4 && (
          <Button
            className="default right"
            onClick={handleADSubmit}
            disabled={!adParameters.terms || !adParameters.budgetAmount}
          >
            Submit
          </Button>
        )}
      </Modal.Footer>
    </Modal>
  );
}
