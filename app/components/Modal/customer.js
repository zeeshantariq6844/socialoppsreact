import React, { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import TimePicker from 'react-time-picker';
import moment from 'moment';
import InputMask from 'react-input-mask';
import { DATE_FORMAT } from '../../utils/helpers';
import { emailValidate } from './email';

const Customer = ({ data, show, handleShow, handleSubmit, error, updated }) => {
  const currentDate = new Date();
  const [invalidEmail, setInvalidEmail] = useState(false);
  const [customer, setCustomer] = useState(data);

  const handleOnChange = ev => {
    setCustomer({ ...customer, [ev.target.name]: ev.target.value });
  };

  if (updated.id) {
    handleShow();
  }

  const onSelectDate = ev => {
    setCustomer({ ...customer, date: moment(ev).format(DATE_FORMAT) });
  };

  const handleTime = time => {
    setCustomer({ ...customer, time });
  };

  const handleCustomerSubmit = () => {
    handleSubmit(customer);
    setCustomer({
      date: moment(currentDate).format(DATE_FORMAT),
      email: '',
    });
  };

  const handleEmail = () => {
    if (customer.email !== '' && emailValidate(customer.email)) {
      setInvalidEmail(true);
    } else {
      setInvalidEmail(false);
    }
  };
  return (
    <Modal
      show={show}
      onHide={handleShow}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="m-2 text-center">
        {error === true && (
          <div className="alert alert-danger">Customer Update Failed</div>
        )}
        <h3>Customer Card</h3>
      </Modal.Header>
      <Modal.Body className="m-2">
        <div>
          <div className="w-50 pull-left pr-2">
            <form>
              <div className="form-group">
                <h5>Name</h5>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  name="name"
                  placeholder="Enter Full Name"
                  value={customer.name}
                  onChange={handleOnChange}
                />
              </div>
              <div className="form-group">
                <h5>Phone</h5>
                <InputMask
                  type="text"
                  className="form-control"
                  id="phone"
                  name="phone"
                  placeholder="Phone Number"
                  onChange={handleOnChange}
                  mask="999-9999-9999"
                  maskChar=""
                  value={customer.phone}
                />
              </div>
              <div className="form-group">
                <h5>Address</h5>
                <input
                  type="text"
                  className="form-control"
                  id="address"
                  name="address"
                  placeholder="Enter Address"
                  onChange={handleOnChange}
                  value={customer.address}
                />
              </div>
              <div className="form-group">
                <h5>Email address</h5>
                <input
                  type="text"
                  className="form-control"
                  id="email"
                  name="email"
                  placeholder="Enter Email"
                  onChange={handleOnChange}
                  onBlur={handleEmail}
                  value={customer.email}
                />
                {invalidEmail && (
                  <small className="text-danger">Invalid email</small>
                )}
              </div>
            </form>
          </div>
          <div className="w-50 pull-right pl-2">
            <form autoComplete="off">
              <h5>Date</h5>
              <div className="d-flex flex-row">
                <div className="date-range-l">
                  <DatePicker
                    name="date"
                    onChange={onSelectDate}
                    selected={customer.date && new Date(customer.date)}
                    minDate={currentDate}
                    dateFormat="yyyy-MM-dd"
                    className="form-control text-right"
                    autocomplete="false"
                  />
                  <i className="fa fa-calendar" aria-hidden="true" />
                </div>
                <div className="ml-3 mr-3 mt-1"> at </div>
                <div>
                  <TimePicker
                    amPmAriaLabel="Select AM/PM"
                    className="roundedInput"
                    clearIcon={null}
                    onChange={handleTime}
                    value={customer.time}
                  />
                </div>
              </div>
              <div className="form-group mt-3">
                <h5>Notes</h5>
                <textarea
                  type="textbox"
                  rows="8"
                  className="form-control rounded"
                  name="notes"
                  placeholder="Type Your Notes Here"
                  onChange={handleOnChange}
                  value={customer.notes}
                />
              </div>
            </form>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button className="grey" onClick={handleShow}>
          X Close
        </Button>
        <Button
          className="default right"
          onClick={handleCustomerSubmit}
          disabled={
            !customer.name ||
            !customer.phone ||
            !customer.address ||
            !customer.notes ||
            !customer.date ||
            !customer.time ||
            !customer.email
          }
        >
          Save
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default Customer;
