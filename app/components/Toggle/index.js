import React, { useState } from 'react';
import ToggleButton from 'react-toggle-button';

function Toggle({ status, toggle, setToggle, invertAdStatus, onChangeToggle }) {
  
  const onToggle = () => {
    console.log('toggle');
    onChangeToggle();
    
  };
  return (
    <ToggleButton
      inactiveLabel=""
      activeLabel=""
      value={toggle}
      onToggle={onToggle}
      colors={{
        activeThumb: {
          base: '#fff',
        },
        inactiveThumb: {
          base: '#fff',
        },
        active: {
          base: '#3b5998',
          hover: '#3b5998',
        },
        inactive: {
          base: '#bdc3c7',
          hover: '#bdc3c7',
        },
      }}
    />
  );
}

export default Toggle;
