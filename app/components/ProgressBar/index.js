import React, { useState } from 'react';
import 'react-step-progress-bar/styles.css';
import { ProgressBar, Step } from 'react-step-progress-bar';

function Progress({progress}) {
  return (
    <ProgressBar percent={progress} filledBackground="#57d68d">
      <Step transition="scale">
        {({ accomplished }) => (
          <div>
            <i className="fa fa-info fa-2x" />
          </div>
        )}
      </Step>
      <Step transition="scale">
        {({ accomplished }) => (
          <div>
            <i className="fa fa-road fa-2x" />{' '}
          </div>
        )}
      </Step>
      <Step transition="scale">
        {({ accomplished }) => (
          <div>
            <i className="fa fa-globe fa-2x" />{' '}
          </div>
        )}
      </Step>
      <Step transition="scale">
        {({ accomplished }) => (
          <div>
            <i className="fa fa-dollar fa-2x" />{' '}
          </div>
        )}
      </Step>
    </ProgressBar>
  );
}

export default Progress;
