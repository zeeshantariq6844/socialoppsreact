import React from 'react';

function Step4({ handleOnChange, adParameters, handleOnBudgetChange }) {
  return (
    <div>
      <div className="clearfix mb-4">
        <div className="form-group pull-left w-50">
          <h5>Budget</h5>
          <div className="budget clearfix">
            <div className="pull-left">
              <div>
                <input
                  className="mb-4"
                  onChange={handleOnChange}
                  type="radio"
                  name="budget"
                  value="Daily"
                  checked={adParameters.budgetType === 'Daily'}
                />{' '}
                Daily Budget
              </div>
              <div>
                <input
                  className="mb-4"
                  onChange={handleOnChange}
                  type="radio"
                  name="budget"
                  value="Weekly"
                  checked={adParameters.budgetType === 'Weekly'}
                />{' '}
                Weekly Budget
              </div>
              <div>
                <input
                  className="mb-4"
                  onChange={handleOnChange}
                  type="radio"
                  name="budget"
                  value="Monthly"
                  checked={adParameters.budgetType === 'Monthly'}
                />{' '}
                Monthly Budget
              </div>
            </div>
            <div className="pull-right">
              <input
                className="form-control budget-amount"
                onChange={handleOnBudgetChange}
                type="text"
                name="budgetAmount"
                value={adParameters.budgetAmount}
                placeholder="0"
              />
            </div>
          </div>
        </div>
        <div className="form-group pull-right w-50">
          <h5>Payment Details</h5>
          <div className="right-half-modal" />
        </div>
      </div>
      <h5>Agreement</h5>
      <div className="boder p-2 clearfix">
        <div className="ml-4">
          <div>
            <h5>
              By Clicking this I concent to the Dealership Toolkit placing this
              advertisement on our behalf. I understand that I agree to pay for
              the costs of placing this ad.
            </h5>
          </div>
          <div className="sm-font">
            A Links To Other Web Sites clause will inform users that you are not
            responsible for any third party websites that you link to. This kind
            of clause will generally inform users that they are responsible for
            reading and agreeing (or disagreeing) with the Terms and Conditions
            or Privacy Policies of these third parties. If your website or
            mobile app allows users to create content and make that content
            public to other users, a Content section will inform users that they
            own the rights to the content they have created. The “Content”
            clause usually mentions that users must give you (the website or
            mobile app developer) a license so that you can share this content
            on your website/mobile app and to make it available to other users.
          </div>
          <div className="pull-right mt-1">
            I Agree{' '}
            <input
              onChange={handleOnChange}
              type="checkbox"
              name="terms"
              value={adParameters.terms}
              checked={adParameters.terms}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Step4;
