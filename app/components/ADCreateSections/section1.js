import React, { useState } from 'react';
import 'react-step-progress-bar/styles.css';
import Upload from '../Upload';

function Step1({
  item,
  onSelectFile,
  handleFormChange,
  adParameters,
}) {
  const [upload, setUpload] = useState(false);
  const alterUpload = () => {
    setUpload(!upload);
  };
  return (
    <div>
      <div className="clearfix mb-4">
        <div className="ad-unit-price form-group pull-left">
          <h5>Unit Title</h5>
          <input
            className="form-control"
            type="text"
            name="title"
            value={adParameters.title}
            onChange={handleFormChange}
          />
        </div>
        <div className="form-group pull-right">
          <h5>Price</h5>
          <input
            className="form-control"
            type="text"
            name="price"
            value={adParameters.price}
            onChange={handleFormChange}
          />
        </div>
      </div>
      <div className="mb-4">
        <div className={upload === false ? '' : 'd-none'}>
          <center>
            <img src={item.img_src} width="600" height="300" />
          </center>
        </div>
        <div className={upload === true ? '' : 'd-none'}>
          <Upload onSelectFile={onSelectFile} />
        </div>
        <button className="default mt-2" onClick={alterUpload}>
          Upload Mode
        </button>
      </div>
      <div className="mb-4">
        <h5>Special Features </h5>
        <textarea
          rows="5"
          name="description"
          className="form-control"
          value={adParameters.description}
          onChange={handleFormChange}
        />
      </div>
    </div>
  );
}

export default Step1;
