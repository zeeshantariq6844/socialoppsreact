import React, { useState } from 'react';
const searchResults = [];

function Step2({ geolocation, audience, handleOnChange, adParameters }) {
  const [search, setSearch] = useState('');
  const handleSearch = ev => {
    setSearch(ev.target.value);
  };
  return (
    <div>
      <div className="clearfix mb-4">
        <div className="form-group pull-left w-50">
          <h5>Geographic Location</h5>
          <select
            name="geolocation"
            className="loc-select"
            onChange={handleOnChange}
            value={adParameters.geolocation}
          >
            <option className="ml-3" value="nationwide">
              Nationwide
            </option>
            <option value="Country">Country</option>
            <option value="City">City</option>
          </select>
        </div>
        <div className="form-group pull-right w-50">
          <h5>Audience</h5>
          <div className="right-half-modal">
            <div className="create-ad-search">
              <input
                className="form-control"
                type="text"
                placeholder="Search"
                onChange={handleSearch}
              />
              <i className="fa fa-search" aria-hidden="true" />
            </div>
            <form>
              {audience.results &&
                audience.results
                  .filter(item => item.title.toLowerCase().includes(search))
                  .map(item => (
                    <div className="mt-3">
                      <input
                        type="checkbox"
                        onChange={handleOnChange}
                        value={item.title}
                        checked={adParameters.audience.indexOf(item.title) > -1}
                        name="audience"
                      />{' '}
                      {item.title}
                    </div>
                  ))}
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Step2;
