import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import { addDays } from '../../utils/helpers';

function Step3({ handleOnChange, adParameters, handleOnDateRange }) {
  const currentDate = new Date();
  const endDate = addDays(currentDate, 14);
  const onSelectStartDate = ev => {
    handleOnDateRange(ev, 'start');
  };
  const onSelectEndDate = ev => {
    handleOnDateRange(ev, 'end');
  };

  return (
    <div>
      <div className="clearfix mb-4">
        <div className="form-group pull-left w-50">
          <h5>Schedule Length</h5>
          <div className="schedule-length">
            <div className="mt-2">
              <input
                className="mb-3"
                onChange={handleOnChange}
                type="radio"
                name="length"
                value="Ever Green AD"
                checked={adParameters.length === 'Ever Green AD'}
              />{' '}
              Ever Green AD (Always Runs)
            </div>
            <div>
              <input
                className="mb-3"
                onChange={handleOnChange}
                type="radio"
                name="length"
                value="1 Week"
                checked={adParameters.length === '1 Week'}
              />{' '}
              1 Week
            </div>
            <div>
              <input
                className="mb-3"
                onChange={handleOnChange}
                type="radio"
                name="length"
                value="2 Weeks"
                checked={adParameters.length === '2 Weeks'}
              />{' '}
              2 Week
            </div>
            <div>
              <input
                className="mb-3"
                onChange={handleOnChange}
                type="radio"
                name="length"
                value="1 Month"
                checked={adParameters.length === '1 Month'}
              />{' '}
              1 Month
            </div>
            <div>
              <input
                className="mb-3"
                onChange={handleOnChange}
                type="radio"
                name="length"
                value="Fixed Date Range"
                checked={adParameters.length === 'Fixed Date Range'}
              />{' '}
              Fixed Date Range
            </div>
            {adParameters.length === 'Fixed Date Range' && (
              <div className="date-range">
                <div className="date-range-w pr-2">
                  <DatePicker
                    name="date"
                    onChange={onSelectStartDate}
                    selected={new Date(adParameters.starDate)}
                    minDate={currentDate}
                    dateFormat="yyyy-MM-dd"
                    className="form-control text-right"
                  />
                  <i className="fa fa-calendar" aria-hidden="true" />
                </div>
                <div className="pr-2">to</div>
                <div className="date-range-w">
                  <DatePicker
                    name="date"
                    onChange={onSelectEndDate}
                    selected={
                      adParameters.endDate !== ''
                        ? new Date(adParameters.endDate)
                        : endDate
                    }
                    minDate={currentDate}
                    dateFormat="yyyy-MM-dd"
                    className="form-control text-right"
                  />
                  <i className="fa fa-calendar" aria-hidden="true" />
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="form-group pull-right w-50">
          <h5>Placement</h5>
          <div className="right-half-modal">
            <form>
              <div className="mt-3">
                <input
                  className="mb-3"
                  onChange={handleOnChange}
                  type="checkbox"
                  name="placement"
                  value="Facebook"
                  checked={adParameters.placement.indexOf('Facebook') > -1}
                />{ ' '}
                Facebook
              </div>
              <div>
                <input
                  className="mb-3"
                  onChange={handleOnChange}
                  type="checkbox"
                  name="placement"
                  value="Facebook Messenger"
                  checked={adParameters.placement.indexOf('Facebook Messenger') > -1}
                />{' '}
                Facebook Messenger
              </div>
              <div>
                <input
                  className="mb-3"
                  onChange={handleOnChange}
                  type="checkbox"
                  name="placement"
                  value="Instagram"
                  checked={adParameters.placement.indexOf('Instagram') > -1}

                />{' '}
                Instagram
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Step3;
