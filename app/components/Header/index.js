import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { Navbar, Nav, Form, NavItem } from 'react-bootstrap';
import logo from '../../Assets/images/FFF_TSL.png';
import { clearSession } from '../../containers/App/actions';
import { makeSelectAuth } from '../../containers/App/selectors';
import { UserContext } from '../../utils/context';

function Header({ auth, dispatchLogout }) {
  const user = React.useContext(UserContext);

  const logout = () => {
    dispatchLogout();
  };
  return (
    <header>
      <Navbar bg="white">
        <Link to="/">
          <Navbar.Brand>
            <img src={logo} width="100" height="30" alt="" />
          </Navbar.Brand>
        </Link>
        <Nav className="mr-auto">
          <NavItem>
            <NavLink exact to="/">
              My Ads
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink exact to="/Leads">
              My Leads
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink exact to="/Inventory">
              My Inventory
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink exact to="/Appointments">
              My Appointments
            </NavLink>
          </NavItem>
        </Nav>
        <Form inline>
          <div className="welcome">
            <div>{auth.access && <span>Welcome {user}</span>}</div>
          </div>
          <div className="ml-4">
            {!auth.access ? (
              <Link to="/login">Login</Link>
            ) : (
              <Link to="/login" onClick={logout}>
                Logout
              </Link>
            )}
          </div>
        </Form>
      </Navbar>
    </header>
  );
}
const mapStateToProps = createStructuredSelector({
  auth: makeSelectAuth(),
});
const mapDispatchToProps = dispatch => ({
  dispatchLogout: () => dispatch(clearSession()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
