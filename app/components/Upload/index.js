import React, { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';

export default function Upload({ onSelectFile }) {
  const onDrop = useCallback(acceptedFiles => {
    acceptedFiles.forEach(file => {
      onSelectFile(acceptedFiles[0]);

    });
  }, []);

  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    multiple: false,
    onDrop,
  });

  return (
    <div>
      <h5>Upload Photos</h5>
      <section className="upload-box" {...getRootProps()}>
        <div>
          <input {...getInputProps()} />
          <div className="text-center">
            <i className="fa fa-upload mr-2" />
            Drag and Drop Photos or Tap to Upload
          </div>
          <div className="mt-2 font-italic text-info">
            {acceptedFiles.map(file => (
            <li key={file.path}>
              {file.path} - {file.size} bytes
            </li>
            ))}
          </div>
        </div>
      </section>
    </div>
  );
}
