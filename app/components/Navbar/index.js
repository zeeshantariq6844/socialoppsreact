import React from "react";
import "./node_modules/bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";

function HeaderTest() {
  return (
    <Navbar className="container" bg="white">
      <Link to="/">
        <Navbar.Brand>SOCIAL OPPS</Navbar.Brand>
      </Link>
      <Nav className="mr-auto">
        <Nav.Link>
          <Link to="/">My Ads</Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/Leads">My Leads</Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/Inventory">My Inventory</Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/Appointments">My Appointments</Link>
        </Nav.Link>
      </Nav>
      <Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
        <Button variant="outline-info">Search</Button>
        <Link to="/login">
          <Button variant="outline-info" className="ml-2">
            Login
          </Button>
        </Link>
      </Form>
    </Navbar>
  );
}

export default HeaderTest;
