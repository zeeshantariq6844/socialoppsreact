import React, { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import CreateAdd from '../../components/Modal';
import { fetchInventory, fetchAudience, postAd, adPostClear } from './actions';
import {
  makeSelectResults,
  makeSelectAudiences,
  selectAdPostResponse,
} from './selectors';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import { PER_PAGE_NUMBER } from '../../config';
import reducer from './reducer';
import saga from './saga';
import Paginate from '../../components/Common/Paginate';
const offset = 0;
const key = 'inventory';

function MyInventory({
  dispatchLoadInventory,
  dispatchLoadAudience,
  dispatchLoadAdPost,
  dispatchLoadClearAdPost,
  data,
  audience,
  adPostResponse,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const [show, setShow] = useState(false);
  const [inventoryItem, setInventoryItem] = useState({});

  useEffect(() => {
    setShow(adPostResponse.error);
  }, [adPostResponse.success]);

  useEffect(() => {
    dispatchLoadClearAdPost();
    dispatchLoadInventory(offset);
    dispatchLoadAudience();
  }, []);

  const handlePageChange = page => {
    dispatchLoadInventory(page.selected * PER_PAGE_NUMBER);
  };

  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  const handleAd = object => {
    dispatchLoadClearAdPost();
    setInventoryItem(object);
    setShow(!show);
  };
  const handleShow = () => {
    setInventoryItem({});
    setShow(!show);
  };

  const submitAd = adParameters => {
    dispatchLoadAdPost(adParameters);
  };
  return (
    <div className="container">
      <div className="divtable">
        {adPostResponse.success === true && (
          <div className="alert alert-success">Ad Posted Successfully</div>
        )}
        <h2>My Inventory ({data.count})</h2>
        <Table hover>
          <thead>
            <tr>
              <th>Stock No</th>
              <th>
                Unit <a className="pull-right mr-3">Thumbnail</a>
              </th>
              <th>Count</th>
              <th>SellingPrice</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {data.results && data.results.length === 0 && (
              <tr>
                <td colSpan="5" className="text-danger">
                  No record found
                </td>
              </tr>
            )}
            {data.results &&
              data.results.map(item => (
                <tr className="rowClass">
                  <td>{item.stock_no}</td>
                  <td>
                    {item.unit_title}{' '}
                    <img
                      className="pull-right"
                      src={item.img_src}
                      width="100"
                    />
                  </td>
                  <td>{item.count}</td>
                  <td>${formatNumber(item.price)}</td>
                  <td className="buttonRow">
                    <Button className="default" onClick={() => handleAd(item)}>
                      Create Ad
                    </Button>
                  </td>
                </tr>
              ))}
            {show && (
              <CreateAdd
                show={show}
                item={inventoryItem}
                adPostResponse={adPostResponse}
                audience={audience}
                handleShow={handleShow}
                submitAd={submitAd}
              />
            )}
            <tr />
          </tbody>
        </Table>
        <div className="pagination">
          <Paginate count={data.count} handlePageChange={handlePageChange} />
        </div>
      </div>
    </div>
  );
}
const mapStateToProps = createStructuredSelector({
  data: makeSelectResults(),
  audience: makeSelectAudiences(),
  adPostResponse: selectAdPostResponse(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadInventory: data => dispatch(fetchInventory(data)),
  dispatchLoadAudience: () => dispatch(fetchAudience()),
  dispatchLoadAdPost: adParameters => dispatch(postAd(adParameters)),
  dispatchLoadClearAdPost: () => dispatch(adPostClear()),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
export default compose(
  withConnect,
  withRouter,
)(MyInventory);
