import {
  FETCH_INVENTORY,
  FETCH_INVENTORY_FAILED,
  FETCH_INVENTORY_SUCCESS,
  FETCH_AUDIENCES,
  FETCH_AUDIENCES_SUCCESS,
  FETCH_AUDIENCES_FAILED,
  POST_AD,
  POST_AD_FAILED,
  POST_AD_SUCCESS,
  CLEAR_AD_RESPONSE,
} from './constants';

export function fetchInventory(offset) {
  return {
    type: FETCH_INVENTORY,
    offset,
  };
}

export function inventoryLoaded(payload) {
  return {
    type: FETCH_INVENTORY_SUCCESS,
    payload,
  };
}
export function inventoryFailed(error) {
  return {
    type: FETCH_INVENTORY_FAILED,
    error,
  };
}

export function fetchAudience() {
  return {
    type: FETCH_AUDIENCES,
  };
}

export function audiencesLoaded(payload) {
  return {
    type: FETCH_AUDIENCES_SUCCESS,
    payload,
  };
}
export function audienceFailed(error) {
  return {
    type: FETCH_AUDIENCES_FAILED,
    error,
  };
}

export function postAd(adParameters) {
  return {
    type: POST_AD,
    adParameters,
  };
}

export function adPostSuccess(payload) {
  return {
    type: POST_AD_SUCCESS,
    payload,
  };
}
export function adPostFailed(error) {
  return {
    type: POST_AD_FAILED,
    error,
  };
}
export function adPostClear() {
  return {
    type: CLEAR_AD_RESPONSE,
  };
}
