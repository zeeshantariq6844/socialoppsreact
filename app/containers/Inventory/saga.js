import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import {
  API_URL,
  INVENTORY_API,
  AUDIENCES_API,
  AD_POST_API,
} from '../../config';
import { FETCH_INVENTORY, FETCH_AUDIENCES, POST_AD } from './constants';
import {
  inventoryFailed,
  inventoryLoaded,
  audiencesLoaded,
  audienceFailed,
  adPostSuccess,
  adPostFailed,
} from './actions';

export function* getInventory(page) {
  const { offset } = page;
  const requestURL = `${API_URL}${INVENTORY_API}${offset}`;
  try {
    const inventory = yield call(request, requestURL);
    yield put(inventoryLoaded(inventory));
  } catch (err) {
    yield put(inventoryFailed(err));
  }
}

export function* getAudience() {
  const requestURL = `${API_URL}${AUDIENCES_API}`;
  try {
    const audience = yield call(request, requestURL);
    yield put(audiencesLoaded(audience));
  } catch (err) {
    yield put(audienceFailed(err));
  }
}

export function* postAd(payload) {
  const requestURL = `${API_URL}${AD_POST_API}`;
  try {
    const adPostResponse = yield call(
      request,
      requestURL,
      'POST',
      payload.adParameters,
      true,
    );

    yield put(adPostSuccess(adPostResponse));
  } catch (err) {
    yield put(adPostFailed(err));
  }
}

export default function* inventorySaga() {
  yield takeLatest(FETCH_INVENTORY, getInventory);
  yield takeLatest(FETCH_AUDIENCES, getAudience);
  yield takeLatest(POST_AD, postAd);
}
