import produce from 'immer';
import {
  FETCH_INVENTORY_SUCCESS,
  FETCH_AUDIENCES_SUCCESS,
  POST_AD_FAILED,
  POST_AD_SUCCESS,
  CLEAR_AD_RESPONSE,
} from './constants';

// The initial state of the App
export const initialState = {
  data: {},
  audience: {},
  submitAd: {
    success: null,
    error: null,
  }
};

/* eslint-disable default-case, no-param-reassign */
const inventoryReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_INVENTORY_SUCCESS:
        draft.data = action.payload;
        break;
      case FETCH_AUDIENCES_SUCCESS:
        draft.audience = action.payload;
        break;
      case POST_AD_SUCCESS:
        draft.submitAd.success = true;
        draft.submitAd.error = false;
        break;
      case POST_AD_FAILED:
        draft.submitAd.success = false;
        draft.submitAd.error = true;
        break;
      case CLEAR_AD_RESPONSE:
        draft.submitAd.success = null;
        draft.submitAd.error = null;
        break;
    }
  });

export default inventoryReducer;
