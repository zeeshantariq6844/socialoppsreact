import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectInventory = state => state.inventory || initialState;


export const makeSelectResults = () =>
  createSelector(
    makeSelectInventory,
    inventory => inventory.data,
  );

export const makeSelectAudiences = () =>
  createSelector(
    makeSelectInventory,
    inventory => inventory.audience,
  );

export const selectAdPostResponse = () =>
  createSelector(
    makeSelectInventory,
    inventory => inventory.submitAd,
  );
