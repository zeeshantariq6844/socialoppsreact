import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { Table, Button } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import {
  makeSelectResults,
  selectCustomerResponse,
  selectCustomerUpdate,
  makeSelectDelete,
} from './selectors';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import {
  fetchAppointments,
  postAppointment,
  customerClear,
  clearUpdate,
  clearDelete,
  deleteAppointment,
} from './actions';
import Paginate from '../../components/Common/Paginate';
import Customer from '../../components/Modal/customer';
import { PER_PAGE_NUMBER } from '../../config';

const offset = 0;
const key = 'Appointments';
function MyAppointments({
  dispatchLoadAppointment,
  dispatchPostCustomer,
  dispatchClearResponse,
  dispatchClearUpdated,
  dispatchDeleteAppointment,
  dispatchClearDeleted,
  data,
  customerResponse,
  updated,
  deleted,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  if (deleted === true) {
    dispatchLoadAppointment();
    dispatchClearDeleted();
  }

  if (updated.id) {
    dispatchLoadAppointment(offset);
    dispatchClearUpdated();
  }

  const [show, setShow] = useState(false);
  const [del, setDel] = useState(false);
  const [customerData, setCustomerData] = useState({});
  const [pid, setPid] = useState();

  const handleEdit = object => {
    setCustomerData(object);
    dispatchClearResponse();
    setShow(!show);
  };
  const handlePageChange = page => {
    dispatchLoadAppointment(page.selected * PER_PAGE_NUMBER);
  };

  const handleShow = () => {
    setCustomerData({});
    dispatchClearResponse();
    setShow(!show);
  };

  const handleSubmit = customer => {
    dispatchPostCustomer(customer);
  };

  useEffect(() => {
    dispatchLoadAppointment(offset);
    dispatchClearResponse();
    dispatchClearUpdated();
  }, []);

  const onDelete = () => {
    dispatchDeleteAppointment(pid);
    setDel(!del);
  };
  const onClickDelete = id => {
    setDel(!del);
    setPid(id);
  };

  return (
    <div>
      <div className="container">
        <div className="divtable">
          {customerResponse.success === true && (
            <div className="alert alert-success">
              Customer Card Updated Successfully
            </div>
          )}
          <div className="clearfix">
            <div className="">
              <h2>My Appointments ({data.count})</h2>
              <center>
                <div className="bg-light confirm mr-4 mt-2 w-50">
                  <div className={del === true ? '' : 'd-none'}>
                    Are You Sure You Want to Delete this Appointment?
                    <div className="clearfix">
                      <button
                        className="pull-left default bg-danger"
                        onClick={() => setDel(!del)}
                      >
                        No
                      </button>
                      <button className="pull-right default" onClick={onDelete}>
                        Yes
                      </button>
                    </div>
                  </div>
                </div>
              </center>
            </div>
            <div className="pull-right">
              <button className="default btn btn-primary" onClick={handleShow}>
                + Add Appointment
              </button>
            </div>
          </div>
          <Table hover>
            <thead>
              <tr>
                <th>Date</th>
                <th>Time</th>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
              {data.results &&
                data.results.map(item => (
                  <tr className="rowClass">
                    <td>{item.date}</td>
                    <td>{item.time}</td>
                    <td>{item.name}</td>
                    <td>
                      <button
                        className="border-0 rounded btn-danger"
                        onClick={() => onClickDelete(item.id)}
                      >
                        Delete
                      </button>
                    </td>
                    <td className="buttonRow">
                      <Button
                        className="btn-secondary"
                        onClick={() => handleEdit(item)}
                      >
                        Edit
                      </Button>
                    </td>
                  </tr>
                ))}
              <tr />
            </tbody>
          </Table>
          <div className="pagination">
            <Paginate count={data.count} handlePageChange={handlePageChange} />
          </div>
        </div>
      </div>
      {show && (
        <Customer
          data={customerData}
          handleSubmit={handleSubmit}
          show={show}
          handleShow={handleShow}
          error={customerResponse.error}
          updated={updated}
        />
      )}
    </div>
  );
}
const mapStateToProps = createStructuredSelector({
  data: makeSelectResults(),
  customerResponse: selectCustomerResponse(),
  updated: selectCustomerUpdate(),
  deleted: makeSelectDelete(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadAppointment: data => dispatch(fetchAppointments(data)),
  dispatchClearResponse: () => dispatch(customerClear()),
  dispatchClearUpdated: () => dispatch(clearUpdate()),
  dispatchPostCustomer: customer => dispatch(postAppointment(customer)),
  dispatchDeleteAppointment: id => dispatch(deleteAppointment(id)),
  dispatchClearDeleted: () => dispatch(clearDelete()),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
export default compose(
  withConnect,
  withRouter,
)(MyAppointments);
