import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectappointments = state =>
  state.Appointments || initialState;

export const makeSelectResults = () =>
  createSelector(
    makeSelectappointments,
    appointment => appointment.data,
  );

export const selectCustomerResponse = () =>
  createSelector(
    makeSelectappointments,
    Appointments => Appointments.submitCustomer,
  );

export const selectCustomerUpdate = () =>
  createSelector(
    makeSelectappointments,
    status => status.updated,
  );

export const makeSelectDelete = () =>
  createSelector(
    makeSelectappointments,
    Appointments => Appointments.deleted,
  );
