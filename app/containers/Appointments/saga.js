import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import { API_URL, APPOINTMENT_API, PER_PAGE_NUMBER } from '../../config';
import {
  FETCH_APPOINTMENTS,
  POST_APPOINTMENT,
  DELETE_APPOINTMENT,
} from './constants';
import {
  loadAppointments,
  loadFail,
  postAppointmentSuccess,
  postAppointmentFail,
  appointmentDeleteFailed,
  appointmentDeleted,
} from './actions';

export function* getAppointments(page) {
  const { offset } = page;
  const requestURL = `${API_URL}${APPOINTMENT_API}?offset=${offset}&limit=${PER_PAGE_NUMBER}`;
  try {
    const Appointments = yield call(request, requestURL);
    yield put(loadAppointments(Appointments));
  } catch (err) {
    yield put(loadFail(err));
  }
}

export function* putAppointment(payload) {
  const requestURL = `${API_URL}${APPOINTMENT_API}`;

  if (payload.customerData.id) {
    try {
      const { id } = payload.customerData;
      delete payload.customerData.id;
      const customer = yield call(
        request,
        `${requestURL + id}/`,
        'PUT',
        JSON.stringify(payload.customerData),
      );
      yield put(postAppointmentSuccess(customer));
    } catch (err) {
      yield put(postAppointmentFail(err));
    }
  } else {
    try {
      const customer = yield call(
        request,
        requestURL,
        'POST',
        JSON.stringify(payload.customerData),
      );
      yield put(postAppointmentSuccess(customer));
    } catch (err) {
      yield put(postAppointmentFail(err));
    }
  }
}

export function* deleteAppointment(payload) {
  const id = payload.data;
  const requestURL = `${API_URL}${APPOINTMENT_API}${id}`;
  try {
    const deleted = yield call(request, requestURL, 'DELETE');
    yield put(appointmentDeleted(deleted));
  } catch (err) {
    yield put(appointmentDeleteFailed(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* appointmentSaga() {
  yield takeLatest(FETCH_APPOINTMENTS, getAppointments);
  yield takeLatest(POST_APPOINTMENT, putAppointment);
  yield takeLatest(DELETE_APPOINTMENT, deleteAppointment);
}
