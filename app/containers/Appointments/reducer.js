/* eslint-disable default-case */
import produce from 'immer';
import {
  FETCH_APPOINTMENTS_SUCCESS,
  POST_APPOINTMENT_SUCCESS,
  POST_APPOINTMENT_FAIL,
  CLEAR_CUSTOMER_RESPONSE,
  CLEAR_CUSTOMER_UPDATE,
  DELETE_APPOINTMENT_SUCCESS,
  CLEAR_DELETE,
} from './constants';

// The initial state of the App
export const initialState = {
  data: {},
  submitCustomer: {
    success: null,
    error: null,
  },
  updated: {},
  deleted: null,
};

const appointmentReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_APPOINTMENTS_SUCCESS:
        draft.data = action.payload;
        break;
      case POST_APPOINTMENT_SUCCESS:
        draft.submitCustomer.success = true;
        draft.submitCustomer.error = false;
        draft.updated = action.payload;
        break;
      case POST_APPOINTMENT_FAIL:
        draft.submitCustomer.success = false;
        draft.submitCustomer.error = true;
        break;
      case CLEAR_CUSTOMER_RESPONSE:
        draft.submitCustomer.success = null;
        draft.submitCustomer.error = null;
        break;
      case CLEAR_CUSTOMER_UPDATE:
        draft.updated = {};
        break;
      case DELETE_APPOINTMENT_SUCCESS:
        draft.deleted = true;
        break;
      case CLEAR_DELETE:
        draft.deleted = null;
        break;
    }
  });

export default appointmentReducer;
