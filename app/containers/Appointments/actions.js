import {
  FETCH_APPOINTMENTS,
  FETCH_APPOINTMENTS_SUCCESS,
  FETCH_APPOINTMENTS_FAIL,
  POST_APPOINTMENT,
  POST_APPOINTMENT_FAIL,
  POST_APPOINTMENT_SUCCESS,
  CLEAR_CUSTOMER_RESPONSE,
  CLEAR_CUSTOMER_UPDATE,
  DELETE_APPOINTMENT,
  DELETE_APPOINTMENT_SUCCESS,
  DELETE_APPOINTMENT_FAILED,
  CLEAR_DELETE,
} from './constants';

export function fetchAppointments(offset) {
  return {
    type: FETCH_APPOINTMENTS,
    offset,
  };
}
export function loadAppointments(payload) {
  return {
    type: FETCH_APPOINTMENTS_SUCCESS,
    payload,
  };
}
export function loadFail(payload) {
  return {
    type: FETCH_APPOINTMENTS_FAIL,
    payload,
  };
}

export function postAppointment(customerData) {
  return {
    type: POST_APPOINTMENT,
    customerData,
  };
}
export function postAppointmentSuccess(payload) {
  return {
    type: POST_APPOINTMENT_SUCCESS,
    payload,
  };
}
export function postAppointmentFail(payload) {
  return {
    type: POST_APPOINTMENT_FAIL,
    payload,
  };
}

export function customerClear() {
  return {
    type: CLEAR_CUSTOMER_RESPONSE,
  };
}
export function clearUpdate() {
  return {
    type: CLEAR_CUSTOMER_UPDATE,
  };
}

export function deleteAppointment(data) {
  return {
    type: DELETE_APPOINTMENT,
    data,
  };
}

export function appointmentDeleted() {
  return {
    type: DELETE_APPOINTMENT_SUCCESS,
  };
}
export function appointmentDeleteFailed() {
  return {
    type: DELETE_APPOINTMENT_FAILED,
  };
}

export function clearDelete() {
  return {
    type: CLEAR_DELETE,
  };
}
