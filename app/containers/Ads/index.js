import React, { useEffect } from 'react';
import { Button } from 'react-bootstrap';
import Adds from 'components/Adds';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
  makeSelectAds,
  makeSelectIsLoading,
  makeSelectDeleted,
  makeSelectInverted,
} from './selectors';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import { makeSelectAuth } from '../App/selectors';
import saga from './saga';
import reducer from './reducer';
import { fetchAds, alterStatus, adDelete, clearDelete } from './actions';
import LoadingIndicator from '../../components/LoadingIndicator';
const key = 'Ads';

function MyAds({
  dispatchLoadAds,
  dispatchChangeAdStatus,
  dispatchDeleteAd,
  dispatchClearDeleted,
  adsData,
  isLoading,
  deleted,
  invert,
}) {
  if (deleted === true) {
    dispatchLoadAds();
    dispatchClearDeleted();
  }
  if (invert === true) {
    dispatchLoadAds();
    dispatchClearDeleted();
  }
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatchLoadAds();
  }, []);

  const invertAdStatus = (id, status) => {
    dispatchChangeAdStatus(id, status);
  };
  const deleteAd = id => {
    dispatchDeleteAd(id);
  };

  return (
    <>
      <div className="create-add">
        <Link to="/Inventory">
          <Button className="default">+ Create Ad</Button>
        </Link>
      </div>
      {isLoading ? (
        <LoadingIndicator />
      ) : (
        <div className="adscontainer">
          {adsData.length === 0 ? (
            <div className="text-danger">No record found </div>
          ) : (
            adsData.map(item => (
              <Adds
                item={item}
                invertAdStatus={invertAdStatus}
                deleteAd={deleteAd}
                className="addStatus"
              />
            ))
          )}
        </div>
      )}
    </>
  );
}

const mapStateToProps = createStructuredSelector({
  auth: makeSelectAuth(),
  adsData: makeSelectAds(),
  isLoading: makeSelectIsLoading(),
  deleted: makeSelectDeleted(),
  invert: makeSelectInverted(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadAds: () => dispatch(fetchAds()),
  dispatchDeleteAd: id => dispatch(adDelete(id)),
  dispatchClearDeleted: () => dispatch(clearDelete()),
  dispatchChangeAdStatus: (id, status) => dispatch(alterStatus(id, status)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(MyAds);
