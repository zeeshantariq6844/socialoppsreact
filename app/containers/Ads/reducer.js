import produce from 'immer';
import {
  FETCH_ADS_SUCCESS,
  FETCH_ADS,
  DELETE_AD_SUCCESS,
  CLEAR_DELETE,
  INVERT_AD_STATUS_SUCCESS,
} from './constants';

// The initial state of the App
export const initialState = {
  addata: [],
  isLoading: false,
  deleted: null,
  inverted: null,
};

/* eslint-disable default-case, no-param-reassign */
const adsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_ADS_SUCCESS:
        draft.addata = action.payload;
        draft.isLoading = false;
        break;
      case DELETE_AD_SUCCESS:
        draft.deleted = true;
        break;
      case CLEAR_DELETE:
        draft.deleted = null;
        draft.inverted = null;
        break;
      case FETCH_ADS:
        draft.isLoading = true;
        break;
      case INVERT_AD_STATUS_SUCCESS:
        draft.inverted = true;
        break;
    }
  });

export default adsReducer;
