import {
  FETCH_ADS,
  FETCH_ADS_SUCCESS,
  FETCH_ADS_FAIL,
  INVERT_AD_STATUS,
  INVERT_AD_STATUS_SUCCESS,
  INVERT_AD_STATUS_FAIL,
  DELETE_AD,
  DELETE_AD_FAILED,
  DELETE_AD_SUCCESS,
  CLEAR_DELETE,
} from './constants';

export function fetchAds(offset) {
  return {
    type: FETCH_ADS,
    offset,
  };
}
export function loadAds(payload) {
  return {
    type: FETCH_ADS_SUCCESS,
    payload,
  };
}
export function loadAdsFail(payload) {
  return {
    type: FETCH_ADS_FAIL,
    payload,
  };
}

export function alterStatus(id, status) {
  return {
    type: INVERT_AD_STATUS,
    id,
    status,
  };
}

export function statusAltered(payload) {
  return {
    type: INVERT_AD_STATUS_SUCCESS,
    payload,
  };
}

export function alterFailed(err) {
  return {
    type: INVERT_AD_STATUS_FAIL,
    err,
  };
}

export function adDelete(id) {
  return {
    type: DELETE_AD,
    id,
  };
}
export function adDeleted(payload) {
  return {
    type: DELETE_AD_SUCCESS,
    payload,
  };
}
export function adDeleteFailed(payload) {
  return {
    type: DELETE_AD_FAILED,
    payload,
  };
}

export function clearDelete() {
  return {
    type: CLEAR_DELETE,
  };
}
