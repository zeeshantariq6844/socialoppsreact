import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const selectGlobal = state => state.Ads || initialState;

export const makeSelectAds = () =>
  createSelector(
    selectGlobal,
    ads => ads.addata,
  );

export const makeSelectIsLoading = () =>
  createSelector(
    selectGlobal,
    ads => ads.isLoading,
  );

export const makeSelectDeleted = () =>
  createSelector(
    selectGlobal,
    ads => ads.deleted,
  );

export const makeSelectInverted = () =>
  createSelector(
    selectGlobal,
    ads => ads.inverted,
  );
