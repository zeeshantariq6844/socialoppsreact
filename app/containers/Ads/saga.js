import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import { API_URL, ADS_API } from '../../config';
import { FETCH_ADS, INVERT_AD_STATUS, DELETE_AD } from './constants';
import {
  loadAds,
  loadAdsFail,
  adDeleteFailed,
  adDeleted,
  statusAltered,
  alterFailed,
} from './actions';

export function* getAds() {
  const requestURL = `${API_URL}${ADS_API}`;
  try {
    const Ads = yield call(request, requestURL);
    yield put(loadAds(Ads));
  } catch (err) {
    yield put(loadAdsFail(err));
  }
}

export function* invertAd(payload) {
  const { id } = payload.id;
  payload.id.ad_status = payload.status;
  delete payload.id.inventory_id;
  delete payload.id.user_id;
  delete payload.id.photo;
  delete payload.id.local_photo;
  const requestURL = `${API_URL}${ADS_API}${id}/`;
  try {
    const invert = yield call(
      request,
      requestURL,
      'PUT',
      JSON.stringify(payload.id),
    );
    yield put(statusAltered(invert));
  } catch (err) {
    yield put(alterFailed(err));
  }
}

export function* deleteAd(payload) {
  const { id } = payload;
  const requestURL = `${API_URL}${ADS_API}${id}`;
  try {
    const deleted = yield call(request, requestURL, 'DELETE');
    yield put(adDeleted(deleted));
  } catch (err) {
    yield put(adDeleteFailed(err));
  }
}

export default function* appointmentSaga() {
  yield takeLatest(FETCH_ADS, getAds);
  yield takeLatest(INVERT_AD_STATUS, invertAd);
  yield takeLatest(DELETE_AD, deleteAd);
}
