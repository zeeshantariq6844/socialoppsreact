import {
  FETCH_LEADS,
  FETCH_LEADS_FAILED,
  FETCH_LEADS_SUCCESS,
  FILTER_LEADS,
  FILTER_LEADS_SUCCESS,
  FILTER_LEADS_FAILED,
  DELETE_LEAD,
  DELETE_LEAD_FAILED,
  DELETE_LEAD_SUCCESS,
  CLEAR_DELETE,
} from './constants';

export function fetchLeads(offset) {
  return {
    type: FETCH_LEADS,
    offset,
  };
}

export function leadsLoaded(payload) {
  return {
    type: FETCH_LEADS_SUCCESS,
    payload,
  };
}
export function leadsFailed(error) {
  return {
    type: FETCH_LEADS_FAILED,
    error,
  };
}

export function filterLeads(payload) {
  return {
    type: FILTER_LEADS,
    payload,
  };
}

export function leadsFiltered(payload) {
  return {
    type: FILTER_LEADS_SUCCESS,
    payload,
  };
}
export function leadsFilterFailed(error) {
  return {
    type: FILTER_LEADS_FAILED,
    error,
  };
}

export function deleteLead(id) {
  return {
    type: DELETE_LEAD,
    id,
  };
}

export function leadDeleteSuccess(payload) {
  return {
    type: DELETE_LEAD_SUCCESS,
    payload,
  };
}
export function leadDeleteFailed(error) {
  return {
    type: DELETE_LEAD_FAILED,
    error,
  };
}

export function clearDelete(error) {
  return {
    type: CLEAR_DELETE,
    error,
  };
}
