import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { fetchLeads, filterLeads, deleteLead, clearDelete } from './actions';
import { makeSelectResults, makeSelectDelete } from './selectors';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import 'react-datepicker/dist/react-datepicker.css';
import saga from './saga';
import Paginate from '../../components/Common/Paginate';
import { addDays, DATE_FORMAT } from '../../utils/helpers';
import { PER_PAGE_NUMBER } from '../../config';

const offset = 0;
const key = 'leads';
// let pid = null;
function MyLeads({
  dispatchLoadLeads,
  dispatchFilterResults,
  dispatchDeleteLead,
  dispatchClearDeleted,
  data,
  deleted,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  useEffect(() => {
    dispatchLoadLeads(offset);
  }, []);
  if (deleted === true) {
    dispatchLoadLeads();
    dispatchClearDeleted();
  }

  const currentDate = new Date();
  const startDate = addDays(currentDate, -30);
  const [show, setShow] = useState(false);
  const [pid, setPid] = useState();

  const [dates, setDates] = useState({
    endDate: moment(currentDate).format(DATE_FORMAT),
    startDate: moment(startDate).format(DATE_FORMAT),
  });

  const onDelete = () => {
    dispatchDeleteLead(pid);
    setShow(!show);
  };
  const onClickDelete = id => {
    setShow(!show);
    setPid(id);
  };

  const onDateChange = (val, type) => {
    const newDates = { ...dates, [type]: moment(val).format(DATE_FORMAT) };
    dispatchFilterResults(newDates);
    setDates(newDates);
  };
  const handlePageChange = page => {
    dispatchLoadLeads(page.selected * PER_PAGE_NUMBER);
  };
  return (
    <div>
      <div className="clearfix">
        <div className="d-flex flex-row-reverse pull-right mr-3">
          <div className="date-range-l">
            <DatePicker
              name="endDate"
              onChange={val => onDateChange(val, 'endDate')}
              selected={new Date(dates.endDate)}
              dateFormat="yyyy-MM-dd"
              className="form-control text-right"
              minDate={addDays(dates.startDate, 0)}
            />
            <i className="fa fa-calendar" aria-hidden="true" />
          </div>
          <div className="pr-2 pl-2 pt-1">To</div>
          <div className="date-range-l">
            <DatePicker
              name="startDate"
              onChange={val => onDateChange(val, 'startDate')}
              selected={
                dates.startDate !== '' ? new Date(dates.startDate) : startDate
              }
              dateFormat="yyyy-MM-dd"
              className="form-control text-right"
              maxDate={addDays(dates.endDate, 0)}
            />
            <i className="fa fa-calendar" aria-hidden="true" />
          </div>
        </div>
      </div>
      <div className="container">
        <div>
          <div />
          <div className="divtable">
            <h2>My Leads ({data.count})</h2>
            <center>
              <div className="bg-light confirm mr-4 mt-2 w-50">
                <div className={show === true ? '' : 'd-none'}>
                  Are You Sure You Want to Delete this Lead?
                  <div className="clearfix">
                    <button
                      className="pull-left default bg-danger"
                      onClick={() => setShow(false)}
                    >
                      No
                    </button>
                    <button className="pull-right default" onClick={onDelete}>
                      Yes
                    </button>
                  </div>
                </div>
              </div>
            </center>
            <Table>
              <thead className="tablehead">
                <tr>
                  <th>Date</th>
                  <th>Customer Name</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Units of Interest</th>
                </tr>
              </thead>
              <tbody>
                {data.results && data.results.length === 0 && (
                  <tr>
                    <td colSpan="5" className="text-danger">
                      No record found
                    </td>
                  </tr>
                )}
                {data.results &&
                  data.results.map(item => (
                    <tr className="rowClass">
                      <td>{item.date}</td>
                      <td className="font-weight-bold">{item.customer_name}</td>
                      <td>{item.phone}</td>
                      <td>{item.email}</td>
                      <td>{item.unit_of_interest}</td>
                      <td>
                        <button
                          className="border-0 rounded btn-danger"
                          onClick={() => onClickDelete(item.id)}
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  ))}
                <tr />
              </tbody>
            </Table>
            <div className="pagination">
              <Paginate
                count={data.count}
                handlePageChange={handlePageChange}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
const mapStateToProps = createStructuredSelector({
  data: makeSelectResults(),
  deleted: makeSelectDelete(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadLeads: data => dispatch(fetchLeads(data)),
  dispatchFilterResults: dates => dispatch(filterLeads(dates)),
  dispatchDeleteLead: id => dispatch(deleteLead(id)),
  dispatchClearDeleted: () => dispatch(clearDelete()),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
export default compose(
  withConnect,
  withRouter,
)(MyLeads);
