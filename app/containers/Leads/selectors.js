import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectLead = state => state.leads || initialState;

export const makeSelectResults = () =>
  createSelector(
    makeSelectLead,
    lead => lead.data,
  );

export const makeSelectDelete = () =>
  createSelector(
    makeSelectLead,
    lead => lead.deleted,
  );
