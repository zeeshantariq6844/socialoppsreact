import produce from 'immer';
import {
  FETCH_LEADS_SUCCESS,
  FILTER_LEADS_SUCCESS,
  DELETE_LEAD_SUCCESS,
  CLEAR_DELETE,
} from './constants';

// The initial state of the App
export const initialState = {
  data: {},
  deleted: null,
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_LEADS_SUCCESS:
        draft.data = action.payload;
        break;
      case FILTER_LEADS_SUCCESS:
        draft.data.results = action.payload;
        break;
      case DELETE_LEAD_SUCCESS:
        draft.deleted = true;
        break;
      case CLEAR_DELETE:
        draft.deleted = null;
        break;
    }
  });

export default homeReducer;
