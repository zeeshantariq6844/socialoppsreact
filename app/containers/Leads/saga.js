import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import { API_URL, LEADS_API, PER_PAGE_NUMBER } from '../../config';
import { FETCH_LEADS, FILTER_LEADS, DELETE_LEAD } from './constants';
import {
  leadsFailed,
  leadsLoaded,
  leadsFiltered,
  leadsFilterFailed,
  leadDeleteFailed,
  leadDeleteSuccess,
} from './actions';

export function* getLeads(data) {
  const { offset } = data;
  const requestURL = `${API_URL}${LEADS_API}`;
  try {
    const leads = yield call(
      request,
      `${requestURL}?offset=${offset}&limit=${PER_PAGE_NUMBER}`,
    );
    yield put(leadsLoaded(leads));
  } catch (err) {
    yield put(leadsFailed(err));
  }
}

export function* filterLeads(data) {
  const requestURL = `${API_URL}${LEADS_API}`;
  try {
    const filteredLeads = yield call(
      request,
      requestURL,
      'POST',
      JSON.stringify(data.payload),
    );
    yield put(leadsFiltered(filteredLeads));
  } catch (err) {
    yield put(leadsFilterFailed(err));
  }
}

export function* leadDelete(payload) {
  const { id } = payload;
  const requestURL = `${API_URL}${LEADS_API}${id}/`;
  try {
    const deleted = yield call(request, requestURL, 'DELETE');
    yield put(leadDeleteSuccess(deleted));
  } catch (err) {
    yield put(leadDeleteFailed(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* inventorySaga() {
  yield takeLatest(FETCH_LEADS, getLeads);
  yield takeLatest(FILTER_LEADS, filterLeads);
  yield takeLatest(DELETE_LEAD, leadDelete);
}
