import produce from 'immer';
import {
  FETCH_TOKEN_SUCCESS,
  LOGOUT,
  FETCH_TOKEN_LOCAL_SUCCESS,
  REFRESH_TOKEN_FAILED,
  REFRESH_TOKEN_SUCCESS,
  FETCH_TOKEN_FAILED,
  CLEAR_LOGIN_ERROR,
} from './constants';
export const initialState = {
  loginFail: null,
  auth: {},
  tokenRefresh: {
    error: null,
    success: null,
  },
};

const appReducer = (state = initialState, action) =>
  produce(state, draft => {
    // eslint-disable-next-line default-case
    switch (action.type) {
      case FETCH_TOKEN_SUCCESS:
        // eslint-disable-next-line no-param-reassign
        draft.auth = action.payload;
        draft.tokenRefresh.error = null;
        draft.tokenRefresh.success = null;
        break;
      case FETCH_TOKEN_FAILED:
        draft.loginFail = 'No active account found with the given credentials';
        break;
      case FETCH_TOKEN_LOCAL_SUCCESS:
        draft.auth = action.payload;
        break;
      case REFRESH_TOKEN_SUCCESS:
        draft.auth = action.payload;
        draft.tokenRefresh.error = false;
        draft.tokenRefresh.success = true;
        break;
      case REFRESH_TOKEN_FAILED:
        draft.tokenRefresh.error = true;
        draft.tokenRefresh.success = false;
        break;
      case LOGOUT:
        draft.auth = {};
        draft.loginFail = null;
        break;
      case CLEAR_LOGIN_ERROR:
        draft.loginFail = null;
        break;
    }
  });

export default appReducer;
