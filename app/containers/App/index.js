import React, { useEffect } from 'react';
import '../../Assets/css/main.css';
import 'font-awesome/css/font-awesome.min.css';
import { Switch, Route } from 'react-router-dom';
import MyAds from 'containers/Ads';
import MyLeads from 'containers/Leads';
import MyInventory from 'containers/Inventory';
import MyAppointments from 'containers/Appointments';
import Login from 'containers/Login/';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Header from 'components/Header';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { fetchTokenLocal, refreshToken, clearSession } from './actions';
import { useInjectReducer } from '../../utils/injectReducer';
import reducer from './reducer';
import { useInjectSaga } from '../../utils/injectSaga';
import saga from './saga';
import { makeSelectAuth, makeSelectUser, makeSelectRefresh } from './selectors';
import 'bootstrap/dist/css/bootstrap.min.css';
import PrivateRoute from '../../utils/PrivateRoute';
import { UserContext } from '../../utils/context';

const key = 'global';
function App({
  dispatchFetchTokenLocal,
  dispatchRefreshToken,
  dispatchLogout,
  user,
  tokenResponse,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const tokenRefresh = token => {
    dispatchRefreshToken(token);
  };
  useEffect(() => {
    dispatchFetchTokenLocal();
  }, []);
  return (
    <div>
      <UserContext.Provider value={user}>
        <Header />
        <div className="container">
          <Switch>
            <PrivateRoute
              tokenRefresh={tokenRefresh}
              tokenResponse={tokenResponse}
              dispatchLogout={dispatchLogout}
              exact
              path="/"
              component={MyAds}
            />
            <Route
              tokenRefresh={tokenRefresh}
              tokenResponse={tokenResponse}
              dispatchLogout={dispatchLogout}
              exact
              path="/login"
              component={Login}
            />
            <PrivateRoute
              tokenRefresh={tokenRefresh}
              tokenResponse={tokenResponse}
              dispatchLogout={dispatchLogout}
              exact
              path="/leads"
              component={MyLeads}
            />
            <PrivateRoute
              tokenRefresh={tokenRefresh}
              tokenResponse={tokenResponse}
              dispatchLogout={dispatchLogout}
              exact
              path="/inventory"
              component={MyInventory}
            />
            <PrivateRoute
              tokenRefresh={tokenRefresh}
              tokenResponse={tokenResponse}
              dispatchLogout={dispatchLogout}
              exact
              path="/appointments"
              component={MyAppointments}
            />
            <Route path="" component={NotFoundPage} />
          </Switch>
        </div>
      </UserContext.Provider>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  auth: makeSelectAuth(),
  user: makeSelectUser(),
  tokenResponse: makeSelectRefresh(),
});

const mapDispatchToProps = dispatch => ({
  dispatchFetchTokenLocal: () => dispatch(fetchTokenLocal()),
  dispatchRefreshToken: token => dispatch(refreshToken(token)),
  dispatchLogout: () => dispatch(clearSession()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
