import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import { API_URL, TOKEN_API, REFRESH_TOKEN_API } from '../../config';
import { FETCH_TOKEN, FETCH_TOKEN_LOCAL, CLEAR_STORAGE } from './constants';
import {
  tokenLoaded,
  tokenLoadedFailed,
  fetchTokenLocalLoaded,
  logout,
  refreshTokenSuccess,
  refreshTokenFailed,
} from './actions';

// Get token from login api and store in local storage
export function* getToken(login) {
  const requestURL = `${API_URL}${TOKEN_API}`;
  try {
    const tokenResponse = yield call(
      request,
      requestURL,
      'POST',
      JSON.stringify(login.payload),
    );

    const dt = new Date();
    dt.setSeconds(dt.getSeconds() + tokenResponse.expiry);
    tokenResponse.expiry = dt;
    localStorage.setItem('authentication', JSON.stringify(tokenResponse));
    yield put(tokenLoaded(tokenResponse));
  } catch (err) {
    yield put(tokenLoadedFailed(err));
  }
}

export function* refreshToken(token) {
  const requestURL = `${API_URL}${REFRESH_TOKEN_API}`;
  const refresh = { refresh: token.payload };
  try {
    const refreshedToken = yield call(
      request,
      requestURL,
      'POST',
      JSON.stringify(refresh),
    );

    const dt = new Date();
    dt.setSeconds(dt.getSeconds() + refreshedToken.expiry);
    const tokenResponse = JSON.parse(localStorage.getItem('authentication'));
    tokenResponse.expiry = dt;
    tokenResponse.access = refreshedToken.access;
    tokenResponse.refresh = refreshedToken.refresh;
    localStorage.setItem('authentication', JSON.stringify(tokenResponse));
    yield put(refreshTokenSuccess(tokenResponse));
  } catch (err) {
    yield put(refreshTokenFailed(err));
  }
}

// get token from Local Storage
export function* getUserToken() {
  try {
    const authentication = localStorage.getItem('authentication');
    if (authentication) {
      yield put(fetchTokenLocalLoaded(JSON.parse(authentication)));
    }
  } catch (err) {}
}

// Clear Local Storage
export function* clearStorage() {
  try {
    localStorage.removeItem('authentication');
    yield put(logout());
  } catch (err) {}
}

export function* test(payload, param) {}
export default function* appSaga() {
  yield takeLatest(FETCH_TOKEN, getToken);
  yield takeLatest(FETCH_TOKEN_LOCAL, getUserToken);
  yield takeLatest(CLEAR_STORAGE, clearStorage);
  yield takeLatest('TEST', test);
  yield takeLatest('REFRESH_TOKEN', refreshToken);
}
