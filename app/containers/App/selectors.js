import { createSelector } from 'reselect';
import { initialState } from './reducer';
const selectApp = state => state.global || initialState;

export const makeSelectAuth = () =>
  createSelector(
    selectApp,
    AppState => AppState.auth,
  );

export const makeSelectUser = () =>
  createSelector(
    makeSelectAuth(),
    auth => auth.username,
  );

export const makeSelectRefresh = () =>
  createSelector(
    selectApp,
    refresh => refresh.tokenRefresh,
  );
export const makeSelectLoginError = () =>
  createSelector(
    selectApp,
    error => error.loginFail,
  );
