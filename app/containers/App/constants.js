export const FETCH_TOKEN = 'FETCH_TOKEN';
export const FETCH_TOKEN_SUCCESS = 'FETCH_TOKEN_SUCCESS';
export const FETCH_TOKEN_FAILED = 'FETCH_TOKEN_FAILED';
export const FETCH_TOKEN_LOCAL = 'FETCH_TOKEN_LOCAL';
export const FETCH_TOKEN_LOCAL_SUCCESS = 'FETCH_TOKEN_LOCAL_SUCCESS';
export const CLEAR_STORAGE = 'CLEAR_STORAGE';
export const LOGOUT = 'LOGOUT';
export const REFRESH_TOKEN = 'REFRESH_TOKEN';
export const REFRESH_TOKEN_SUCCESS = 'REFRESH_TOKEN_SUCCESS';
export const REFRESH_TOKEN_FAILED = 'REFRESH_TOKEN_FAILED';
export const CLEAR_LOGIN_ERROR = 'CLEAR_LOGIN_ERROR';
