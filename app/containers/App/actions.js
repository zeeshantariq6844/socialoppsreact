import {
  FETCH_TOKEN,
  FETCH_TOKEN_SUCCESS,
  FETCH_TOKEN_FAILED,
  FETCH_TOKEN_LOCAL,
  FETCH_TOKEN_LOCAL_SUCCESS,
  CLEAR_STORAGE,
  REFRESH_TOKEN,
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_FAILED,
  LOGOUT,
  CLEAR_LOGIN_ERROR,
} from './constants';

export function fetchToken(payload) {
  return {
    type: FETCH_TOKEN,
    payload,
  };
}

export function tokenLoaded(payload) {
  return {
    type: FETCH_TOKEN_SUCCESS,
    payload,
  };
}
export function tokenLoadedFailed(payload) {
  return {
    type: FETCH_TOKEN_FAILED,
    payload,
  };
}

// get token from localStorage
export function fetchTokenLocal() {
  return {
    type: FETCH_TOKEN_LOCAL,
  };
}
export function fetchTokenLocalLoaded(payload) {
  return {
    type: FETCH_TOKEN_LOCAL_SUCCESS,
    payload,
  };
}

// Clear Local Storage and State
export function clearSession() {
  return {
    type: CLEAR_STORAGE,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function refreshToken(payload) {
  return {
    type: REFRESH_TOKEN,
    payload,
  };
}

export function refreshTokenSuccess(payload) {
  return {
    type: REFRESH_TOKEN_SUCCESS,
    payload,
  };
}

export function refreshTokenFailed(payload) {
  return {
    type: REFRESH_TOKEN_FAILED,
    payload,
  };
}

export function clearLoginError(payload) {
  return {
    type: CLEAR_LOGIN_ERROR,
    payload,
  };
}
