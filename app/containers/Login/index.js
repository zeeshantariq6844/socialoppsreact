import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { fetchToken, clearLoginError } from '../App/actions';
import {
  makeSelectAuth,
  makeSelectRefresh,
  makeSelectLoginError,
} from '../App/selectors';

function Login({
  dispatchFetchToken,
  dispatchClearLoginError,
  history,
  auth,
  loginError,
}) {
  if (auth.access) {
    history.push('/');
  }

  const [credentials, setForm] = useState({
    username: '',
    password: '',
  });
  const [Error, setError] = useState({});

  const handleOnChange = ev => {
    setForm({ ...credentials, [ev.target.name]: ev.target.value });
    setError(null);
    dispatchClearLoginError();
  };

  const handleOnSubmit = ev => {
    ev.preventDefault();
    if (
      credentials.username.length === 0 ||
      credentials.password.length === 0
    ) {
      setError(true);
    } else {
      dispatchFetchToken(credentials);
    }
  };

  return (
    <div>
      <div className="container">
        <div
          id="login-row"
          className="row justify-content-center align-items-center"
        >
          <div id="login-column" className="col-md-6">
            <div id="login-box" className="col-md-12">
              <form id="login-form" className="form" onSubmit={handleOnSubmit}>
                <h3 className="text-center text-info">Login</h3>
                <div className={Error === true ? '' : 'd-none'}>
                  <div className="alert alert-danger">
                    Please Enter both Username and Password
                  </div>
                </div>
                <div className={loginError !== null ? '' : 'd-none'}>
                  <div className="alert alert-danger">{loginError}</div>
                </div>
                <div className="form-group">
                  <label htmlFor="username">Username:</label>
                  <br />
                  <input
                    type="text"
                    name="username"
                    id="username"
                    className="form-control"
                    onChange={handleOnChange}
                    value={useState.username}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Password:</label>
                  <br />
                  <input
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                    onChange={handleOnChange}
                    value={useState.password}
                  />
                </div>

                <div className="">
                  <br />
                  <button
                    type="submit"
                    name="submit"
                    className="btn default btn-md"
                  >
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  auth: makeSelectAuth(),
  loginError: makeSelectLoginError(),
});

const mapDispatchToProps = dispatch => ({
  dispatchFetchToken: data => dispatch(fetchToken(data)),
  dispatchClearLoginError: () => dispatch(clearLoginError()),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(Login);
